import { Ionicons } from "@expo/vector-icons";
import { AppLoading } from "expo";
import { Asset } from "expo-asset";
import * as Font from "expo-font";
import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { Platform, StyleSheet, View } from "react-native";
import { Provider as PaperProvdier, DefaultTheme } from "react-native-paper";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import Colors from "./constants/Colors";
import useCachedResources from "./hooks/useCachedResources";
import useColorScheme from "./hooks/useColorScheme";
import Navigation from "./navigation";
import AppNavigator from "./navigation/AppNavigator";
import { store, persistor } from "./redux/store";

export default function App() {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return <AppLoading startAsync={useCachedResources} />;
  } else {
    // return (
    //   <SafeAreaProvider>
    //     <Navigation colorScheme={colorScheme} />
    //     <StatusBar />
    //   </SafeAreaProvider>
    // );

    const theme = {
      ...DefaultTheme,
      roundness: 2,
      colors: {
        ...DefaultTheme.colors,
        primary: Colors.primary,
        accent: Colors.primaryBorder,
        background: Colors.white,
      },
    };
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <PaperProvdier style={styles.container} theme={theme}>
            <StatusBar barStyle="light-content" />
            <AppNavigator />
          </PaperProvdier>
        </PersistGate>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});

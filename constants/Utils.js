const baseURL = "https://9ec825b5d78f.ngrok.io";
//const baseURL = 'https://iddera.herokuapp.com/'
//const baseURL = "https://admin.ffb.com.ng/";

export default {
  baseURL: `${baseURL}/api`,
  baseEndpoint: baseURL,
};

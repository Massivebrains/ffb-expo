const tintColorLight = "#2f95dc";
const tintColorDark = "#fff";

export default {
  light: {
    text: "#000",
    background: "#fff",
    tint: tintColorLight,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorLight,
  },
  dark2: {
    text: "#fff",
    background: "#000",
    tint: tintColorDark,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorDark,
  },

  primary: "#3C4192",
  primaryBorder: "#5a61ce",
  primaryDark: "#e6ac00",
  white: "#fff",
  tintColor: "#ffc825",
  tabIconDefault: "#686466",
  tabIconSelected: "#ff840d",
  tabBar: "#fff",
  grey: "#edf2f7",
  lightGrey: "#686466",
  background: "#f4f4f6",
  dark: "#22292f",
  orange: "#ff840d",
};

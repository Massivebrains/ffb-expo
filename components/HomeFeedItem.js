/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useRef } from "react";
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from "react-native";

import Colors from "../constants/Colors";

export default function HomeFeedItem(props) {
  const post = props.post.item;
  return (
    <TouchableOpacity activeOpacity={0.9} onPress={props.onPress}>
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            source={require("../assets/images/ffb.png")}
            style={{ width: 50, height: 50, borderRadius: 25 }}
          />
          <View style={styles.titleWrapper}>
            <Text style={styles.title}>{post.title}</Text>
            <Text style={styles.date}>{post.publish_date}</Text>
          </View>
        </View>
        <Image
          source={{ uri: "https://via.placeholder.com/300x300" }}
          style={{
            width: "100%",
            height: 100,
            borderRadius: 10,
            marginBottom: 10,
          }}
        />
        <Text numberOfLines={2} style={styles.description}>
          {post.excerpt}
        </Text>
        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Post", { post })}
          >
            <View>
              <Text style={styles.followBtnText}>Read More</Text>
            </View>
          </TouchableOpacity>
          <TouchableWithoutFeedback>
            <View style={styles.followBtnWrapper}>
              <Text style={styles.followBtnText}>Follow</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = {
  container: {
    backgroundColor: "white",
    marginVertical: 15,
    width: "90%",
    alignSelf: "center",
    elevation: 5,
    borderRadius: 10,
    padding: 10,
  },
  header: {
    flexDirection: "row",
    marginBottom: 10,
  },
  titleWrapper: {
    flex: 2,
    marginHorizontal: 10,
  },
  title: {
    fontWeight: "bold",
    fontSize: 14,
  },
  date: {
    color: "gray",
    fontWeight: "light",
    fontSize: 11,
    marginTop: 5,
  },
  followBtnWrapper: {
    backgroundColor: "#3C419230",
    height: 20,
    width: 60,
    alignItems: "center",
    borderRadius: 10,
  },
  followBtnText: {
    color: Colors.primary,
  },
  description: {
    color: "#00000099",
    fontSize: 13,
  },
  footer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 10,
  },
};

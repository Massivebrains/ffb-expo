/* eslint-disable prefer-promise-reject-errors */
import { Linking } from "react-native";

import Utils from "../constants/Utils";

export default {
  get: (endpoint = "", token = "") =>
    new Promise(async (resolve, reject) => {
      Linking.canOpenURL(Utils.baseEndpoint).then((connection) => {
        if (!connection) {
          return reject("No Internet Avaliable. Please check your network.");
        }
      });

      const payload = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      };

      // eslint-disable-next-line no-undef
      fetch(`${Utils.baseURL}/${endpoint}`, payload)
        .then((response) => response.json())
        .then((json) => {
          resolve(json);
        })
        .catch((error) => {
          reject(error);
        });
    }),

  post: (endpoint = "", data = {}, token = "") => {
    return new Promise(async (resolve, reject) => {
      Linking.canOpenURL(Utils.baseEndpoint).then((connection) => {
        if (!connection) {
          return reject("No Internet Avaliable. Please check your network.");
        }
      });

      const payload = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(data),
      };

      // eslint-disable-next-line no-undef
      fetch(`${Utils.baseURL}/${endpoint}`, payload)
        .then((response) => response.json())
        .then((json) => {
          resolve(json);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  postFile: (endpoint = "", data = null, token = "") => {
    return new Promise(async (resolve, reject) => {
      const payload = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${token}`,
        },
        body: data,
      };

      // eslint-disable-next-line no-undef
      fetch(`${Utils.baseURL}/${endpoint}`, payload)
        .then((response) => response.json())
        .then((json) => {
          resolve(json);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { Platform, View } from "react-native";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator, BottomTabBar } from "react-navigation-tabs";

import TabBarIcon from "../components/TabBarIcon";
import Colors from "../constants/Colors";
import {
  HomeScreen,
  EntertainmentScreen,
  FootballScreen,
  AccountScreen,
  MessagesScreen,
  PostScreen,
} from "../screens";

const config = Platform.select({
  web: { headerMode: "screen" },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Post: PostScreen
  },
  config
);
HomeStack.navigationOptions = {
  tabBarLabel: <View />,
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === "ios"
          ? `ios-paper${focused ? "" : "-paper"}`
          : "md-paper"
      }
    />
  ),
};
HomeStack.path = "";

const EntertainmentStack = createStackNavigator(
  { 
    Entertainment: EntertainmentScreen, 
    Post: PostScreen 
  },
  config
);
EntertainmentStack.navigationOptions = {
  tabBarLabel: <View />,
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-film" : "md-film"}
      title="Text"
    />
  ),
};

const FootballStack = createStackNavigator(
  { Football: FootballScreen, Post: PostScreen },
  config
);
FootballStack.navigationOptions = {
  tabBarLabel: <View />,
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-football" : "md-football"}
    />
  ),
};
FootballStack.path = "";

const MessagesStack = createStackNavigator(
  { Messages: MessagesScreen },
  config
);
MessagesStack.navigationOptions = {
  tabBarLabel: <View />,
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-chatboxes" : "md-chatboxes"}
    />
  ),
};

MessagesStack.path = "";

const ProfileStack = createStackNavigator({ Profile: AccountScreen }, config);

ProfileStack.navigationOptions = {
  tabBarLabel: <View />,
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-person" : "md-person"}
    />
  ),
};
ProfileStack.path = "";

const TabBarComponent = (props) => <BottomTabBar {...props} />;
const tabNavigator = createBottomTabNavigator(
  {
    HomeStack,
    EntertainmentStack,
    FootballStack,
    MessagesStack,
    ProfileStack,
  },
  {
    tabBarComponent: (props) => (
      <TabBarComponent
        {...props}
        keyboardHidesTabBar={true}
        style={{
          border: 0,
          backgroundColor: Colors.white,
          color: Colors.primary,
          borderTopWidth: 0,
        }}
        labelStyle={{ color: Colors.lightGrey }}
        activeTintColor={Colors.primary}
      />
    ),
  }
);

tabNavigator.path = "";

export default tabNavigator;

import { POSTS } from "../types";

const initialState = {
  home: [],
  entertainments: [],
  football: [],
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case POSTS:
      if (!payload) return { ...state, ...initialState };

      return { ...state, ...payload };

    default:
      return state;
  }
}

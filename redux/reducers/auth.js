import { AUTH, LOGOUT } from '../types';

const initialState = {

    api_token: null,
    user: {},
    name: '',
    type: ''
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case AUTH:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}
/* eslint-disable @typescript-eslint/no-unused-vars */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React, { useEffect, useState } from "react";
import { StyleSheet, View, ScrollView } from "react-native";
import { DotIndicator } from "react-native-indicators";
import { List, Button } from "react-native-paper";
import { NavigationActions } from "react-navigation";
import { useDispatch, useSelector } from "react-redux";

import Colors from "../constants/Colors";
import { AUTH, POSTS } from "../redux/types";

export default function AccountScreen(props) {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function getData() {
      try {
        setLoading(false);
        setUser(auth.user);
        console.log(user);
      } catch (error) {
        setLoading(false);
      }
    }

    getData();
  }, []);

  const logout = async () => {
    dispatch({ type: AUTH, payload: null });
    dispatch({ type: POSTS, payload: null });

    props.navigation.navigate("Auth");
  };

  const changePin = async () => {
    props.navigation.navigate("Pin");
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <DotIndicator size={10} color={Colors.dark} style={{ padding: 5 }} />
      ) : (
        <ScrollView contentContainerStyle={{ paddingTop: 20 }}>
          <List.Item
            left={(props) => (
              <List.Icon {...props} color={Colors.primary} icon="account-box" />
            )}
            title={user.email}
            description="Email Address"
          />
          <List.Item
            left={(props) => (
              <List.Icon
                {...props}
                color={Colors.primary}
                icon="account-card-details"
              />
            )}
            title={user.name}
            description="Name"
          />
          <List.Item
            left={(props) => (
              <List.Icon {...props} color={Colors.primary} icon="phone" />
            )}
            title={user.phone}
            description="Phone Number"
          />
          <List.Item
            left={(props) => (
              <List.Icon {...props} color={Colors.primary} icon="bank" />
            )}
            title={`Chelsea`}
            description="Club"
            titleNumberOfLines={2}
          />
          <List.Item
            left={(props) => (
              <List.Icon {...props} color={Colors.primary} icon="bank" />
            )}
            title={user.username}
            description="Username"
            titleNumberOfLines={2}
          />

          <Button
            uppercase={false}
            mode="contained"
            style={{
              marginTop: 10,
              marginHorizontal: 20,
              borderRadius: 7,
              backgroundColor: "transparent",
              elevation: 0,
            }}
            contentStyle={{ height: 50 }}
            labelStyle={{ fontSize: 16, color: Colors.dark }}
            raised
            onPress={() => logout()}
          >
            Logout
          </Button>
        </ScrollView>
      )}
    </View>
  );
}

AccountScreen.navigationOptions = {
  title: "Profile",
  headerStyle: { backgroundColor: Colors.dark },
  headerTintColor: Colors.background,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
});

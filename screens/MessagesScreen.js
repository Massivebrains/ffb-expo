/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import { View, StyleSheet } from "react-native";
import { DotIndicator } from "react-native-indicators";
import { WebView } from "react-native-webview";

import Colors from "../constants/Colors";

export default function MessagesScreen(props) {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);

    // eslint-disable-next-line no-undef
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  return (
    <View style={styles.container}>
      {loading ? (
        <DotIndicator
          size={10}
          color={Colors.primary}
          style={{ paddingTop: 20 }}
        />
      ) : (
        <WebView
          originWhitelist={["*"]}
          source={{ uri: "https://pusher.com" }}
          scalesPageToFit
          javascriptEnabled
          domStorageEnabled
          startInLoadingState
          mixedContentMode="always"
        />
      )}
    </View>
  );
}

MessagesScreen.navigationOptions = {
  title: "Messages",
  headerStyle: { backgroundColor: Colors.primary },
  headerTintColor: Colors.white,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
});

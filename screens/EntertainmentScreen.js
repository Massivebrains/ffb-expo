/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, FlatList, View } from "react-native";
import { Title, Avatar, Card, Paragraph, Divider } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";

import API from "../components/API";
import HomeFeedItem from "../components/HomeFeedItem";
import Colors from "../constants/Colors";
import { AUTH, POSTS } from "../redux/types";

export default function EntertainmentScreen(props) {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const posts = useSelector((state) => state.posts);
  const setProps = (payload = {}) => dispatch({ type: POSTS, payload });

  useEffect(() => {
    async function getData() {
      try {
        if (posts.entertainments.length > 0) return;
        const response = await API.get("posts", auth.api_token);

        if (response.status === true) {
          const data = response.data;
          setProps({
            home: data,
            entertainments: data,
            football: data,
          });
        }
      } catch (error) {
        console.log(error);
      }
    }

    getData();
  });

  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={{ paddingTop: 30 }}
      >
        <View style={styles.welcomeContainer}>
          <Title style={styles.welcomeText}>Entertaiments</Title>
        </View>
        <FlatList
          data={posts.entertainments}
          renderItem={(post) => (
            <HomeFeedItem post={post} navigation={props.navigation} />
          )}
        />
      </ScrollView>
    </View>
  );
}

EntertainmentScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },

  welcomeText: {
    fontSize: 25,
    marginVertical: 20,
    color: Colors.primary,
  },

  cashWithMe: {
    marginHorizontal: 20,
    alignItems: "center",
    padding: 25,
    elevation: 4,
    borderRadius: 7,
    backgroundColor: Colors.white,
    shadowOffset: { width: 0, height: 0.5, shadowOpacity: 0.3 },
  },

  cashText: {
    textAlign: "center",
    color: Colors.dark,
    fontSize: 16,
  },

  pendingText: {
    textAlign: "center",
    fontSize: 30,
    color: Colors.dark,
  },

  button: {
    flex: 1,
    elevation: 4,
    backgroundColor: Colors.white,
    borderRadius: 8,
    marginBottom: 10,
    color: Colors.dark,
  },

  welcomeContainer: {
    alignItems: "flex-start",
    paddingLeft: 20,
    paddingRight: 20,
  },
});

/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  Image,
} from "react-native";
import { DotIndicator } from "react-native-indicators";
import {
  Title,
  Button,
  Card,
  HelperText,
  TextInput,
  Avatar,
} from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";

import API from "../../components/API";
import Colors from "../../constants/Colors";
import { AUTH } from "../../redux/types";

export default function LoginScreen(props) {
  const dispatch = useDispatch();

  const setProps = (payload = {}) => dispatch({ type: AUTH, payload });

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const login = async () => {
    try {
      setLoading(true);

      const response = await API.post("login", {
        username,
        password,
      });

      if (response.status === true) {
        const data = response.data;
        setProps({
          api_token: data.api_token,
          user: data,
          name: data.first_name,
          type: data.type,
        });
        setLoading(false);
        setPassword("");

        props.navigation.navigate("Main");
      } else {
        setLoading(false);
        setPassword("");
        Alert.alert("Login Failed", response.data.toString());
      }
    } catch (error) {
      setLoading(false);
      Alert.alert("Login Failed", error.toString());
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={{
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Image
          style={styles.avatar}
          source={require("../../assets/images/ffb.png")}
        />

        <Card style={styles.loginContainer}>
          <Title style={{ color: Colors.white, marginBottom: 20 }}>
            Please Login
          </Title>

          <View style={styles.inputBox}>
            <TextInput
              label="Username"
              style={styles.input}
              value={username}
              onChangeText={(text) => setUsername(text)}
              underlineColor="transparent"
            />
          </View>

          <View>
            <TextInput
              label="Password"
              style={styles.input}
              value={password}
              onChangeText={(text) => setPassword(text)}
              underlineColor="transparent"
              autoCompleteType="off"
              secureTextEntry={true}
            />
          </View>

          {loading ? (
            <DotIndicator
              size={10}
              color={Colors.white}
              style={{ padding: 5, marginTop: 50 }}
            />
          ) : (
            <Button
              uppercase={false}
              mode="contained"
              style={{
                marginTop: 30,
                borderRadius: 7,
                elevation: 0,
                backgroundColor: Colors.white,
              }}
              contentStyle={{ height: 50 }}
              labelStyle={{ fontSize: 20, color: Colors.primary }}
              raised
              onPress={() => login()}
            >
              Login
            </Button>
          )}

          <TouchableOpacity style={{ marginTop: 10 }}>
            <Text style={{ textAlign: "center", color: Colors.white }}>
              Don't have an account?
            </Text>
          </TouchableOpacity>
        </Card>
      </ScrollView>
    </View>
  );
}

LoginScreen.navigationOptions = { header: null };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  avatar: {
    flex: 1,
    justifyContent: "center",
    marginTop: "15%",
    marginBottom: 10,
  },

  loginContainer: {
    flex: 5,
    width: "90%",
    elevation: 0.4,
    borderWidth: 0.1,
    borderColor: "rgba(0,0,0,0.2)",
    backgroundColor: Colors.primary,
    borderRadius: 5,
    marginHorizontal: 25,
    padding: 20,
  },

  inputBox: {
    marginBottom: 20,
  },

  input: {
    backgroundColor: Colors.white,
    elevation: 7,
    height: 55,
  },
});

/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect } from "react";
import { StatusBar, ActivityIndicator, View } from "react-native";
import { useSelector } from "react-redux";

export default function AuthLoadingScreen(props) {
  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    props.navigation.navigate(auth.api_token == null ? "Auth" : "Main");
  });

  return (
    <View style={{ flex: 1 }}>
      <ActivityIndicator />
      <StatusBar barStyle="default" />
    </View>
  );
}
